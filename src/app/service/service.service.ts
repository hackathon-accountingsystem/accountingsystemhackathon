import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http:HttpClient) { }
  public showtable(){
    return this.http.get("http://localhost:8080/getData")
  }
  public show(){
    return this.http.get("http://localhost:8080/get")
  }
  public sortparent(){
    return this.http.get("http://localhost:8080/parentId")
  }
  public sortparentdes(){
    return this.http.get("http://localhost:8080/parentDes")
  }
  public sortaccountid(){
    return this.http.get("http://localhost:8080/accountId")
  }
  public sortaccountiddes(){
    return this.http.get("http://localhost:8080/accountIdDes")
  }
  public searchById(body:any){
    return this.http.post("http://localhost:8080/search",body)
  }

  public searchByParent(body:any){
    return this.http.post("http://localhost:8080/searchByParent",body)
  }
  public searchByAccountId(body:any){
    return this.http.post("http://localhost:8080/searchByAccountId",body)
  }
  public limit(body:any){
    return this.http.get("http://localhost:8080/limit?limit="+body)
  }
  public sortaccountnumber(){
    return this.http.get("http://localhost:8080/accountNumber")
  }
  public sortaccountnumberdes(){
    return this.http.get("http://localhost:8080/accountNumberDes")
  }
  public searchByAccountName(body:any){
    return this.http.post("http://localhost:8080/searchByAccountName",body)
  }
  public sortByAccountName(){
    return this.http.get("http://localhost:8080/accountName")
  }
  public sortByAccountNamedes(){
    return this.http.get("http://localhost:8080/accountNameDes")
  }
}
