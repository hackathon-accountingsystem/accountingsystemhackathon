
import { Component, OnInit } from '@angular/core';
import { HomeService } from '../service/service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  view!: any;
sort!:string;

limit:any;
searchByParentId!:string;
search:any;
sortedcolumn! :string;
searchByAccountNumber!:string;
searchByAcountId!:string;
searchByAccountnme!:string;
searchValue:any;
p:any;
message:any;
flag1=0;
flag2=0;
flag3=0;
flag4=0;
flagdis=1;
flag5=0;
sp=0;
sn=0;
snm=0;
spa=0;
  constructor(private home:HomeService) { }

  ngOnInit(): void {
    this.home.show().subscribe((val)=>{
      this.view=val;
      // this.view=JSON.parse(this.view)
      // this.view=JSON.stringify(this.view);
      console.log(this.view);
      
    });
  }

  onClicksearch(){
this.flag2=1;
this.flag3=0;
this.flag4=0;
this.flag5=0;
   }
   onClickSearchByParentId(){
    this.flag3=1;
    this.flag2=0;
    this.flag4=0;
    this.flag5=0;
       }
       onsearchByAccountID(){
        this.flag3=0;
        this.flag2=0;
        this.flag4=1;
        this.flag5=0;
           }
           onSearchByAccountName(){
            this.flag3=0;
            this.flag2=0;
            this.flag4=0;
            this.flag5=1;
               }
       onView(){
        this.flag3=0;
        this.flag2=0;
        this.flag1=0;
        this.message='';
        this.flagdis=1;
        this.flag4=0;
          
           }
  onClickSortparent(){
    this.home.sortparent().subscribe((val)=>{
     this.spa=1;
      this.view=val;
      console.log(this.view)
    })
  }
  onClickSortparentdes(){
    this.home.sortparentdes().subscribe((val)=>{
     this.spa=0;
      this.view=val;
      console.log(this.view)
    })
  }
  onClickSortaccountnumber(){
    this.home.sortaccountnumber().subscribe((val)=>{
     this.sn=1;
      this.view=val;
      
    })
  }
  onClickSortaccountnumberdesc(){
    this.home.sortaccountnumberdes().subscribe((val)=>{
     
      this.view=val;
      this.sn=0;
    })
  }
  onClickSortaccountname(){
    this.home.sortByAccountName().subscribe((val)=>{
     this.snm=1;
      this.view=val;
      
    })
  }
  onClickSortaccountnamedesc(){
    this.home.sortByAccountNamedes().subscribe((val)=>{
     this.snm=0;
      this.view=val;
      
    })
  }
  onClickSortaccountid(){
    this.home.sortaccountid().subscribe((val)=>{
     this.sp=1;
      this.view=val;
      console.log(this.view)
    })
  }
  onClickSortaccountiddes(){
    this.home.sortaccountiddes().subscribe((val)=>{
     this.sp=0;
      this.view=val;
      console.log(this.view)
    })
  }
  onSearchbyacid(){
    
      this.home.searchByAccountId(this.searchByAcountId).subscribe((val)=>{
     this.flag1=1;
        this.searchValue=val;
        console.log(this.searchValue);
        this.flagdis=0;
      })
    }

  
  onSearch(){
    this.home.searchById(this.searchByAccountNumber).subscribe((val)=>{
      this.flag1=1;
      this.searchValue=val;
      if( this.searchValue.length==0){
        this.message="No Data Found" ;
         this.flagdis=0;
       }
    });
  
  }
  onSearchByParent(){
    this.home.searchByParent(this.searchByParentId).subscribe((val)=>{
      this.flag1=1;
      this.searchValue=val;
      if( this.searchValue.length==0){
       this.message="No Data Found" ;
        this.flagdis=0;
      }

      this.flagdis=0;
    });
  
  }
  onClicksearchByAccountName(){
    this.home.searchByAccountName(this.searchByAccountnme).subscribe((val)=>{
      this.flag1=1;
      this.searchValue=val;
      if( this.searchValue.length==0){
        this.message="No Data Found" ;
         this.flagdis=0;
       }
       this.flagdis=0;
    });
  
  }
 
 
  onLimit(event:any){
    this.limit=event.target.value;
 console.log(this.limit);
 
    this.view=[];
    this.home.limit(this.limit).subscribe((val)=>{
this.view=val;
    })
  }
  }
